{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# RICH reconstruction CNN lab"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Cherenkov radiation is a cone of photons produced by particles, as they travel faster than the speed of light in a particular medium. The radius of the emitted cone depends on the particle mass and its momentum.\n",
    "\n",
    "If you must ask, here is the formula that dictates this phaenomenon. Don't worry, you won't need the formula for the lab:\n",
    "\n",
    "<img src=\"images/rich_in_depth.png\" alt=\"rich_in_depth\" width=\"600\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These photons can be detected using several technologies. At LHCb, we will use in the next few years _multianode photomultipliers_ (MaPMT).\n",
    "\n",
    "Placing the MaPMTs in the path of the particle would result in detecting not only the Cherenkov radiation produced from the particle, but would also be disruptive with the particle itself. This is why mirrors reflect the photons and redirect them to the actual detector.\n",
    "\n",
    "One of the mirrors is spherical, whereas the other is planar. This causes deformations in the expected shape of the particle photons.\n",
    "\n",
    "<img src=\"images/rich_reflection.png\" alt=\"rich_reflection\" width=\"600\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you know the basics of the geometry of the detector, here is the produced signal of a simulated single _event_ (bunch crossing) at the LHC:\n",
    "\n",
    "<img src=\"images/scatter_rich1.png\" alt=\"scatter_rich1\" width=\"800\" />\n",
    "\n",
    "* Blue: Detected photons\n",
    "* Red: Projected center of the particle\n",
    "\n",
    "Several things to note here. The detected photons are either detected or not detected, so this is binary information. The particles (red dots) producing Cherenkov radiation are also presented in this image. Can you detect the circumference of photons surrounding it with your bare eye?\n",
    "\n",
    "You may also notice there are other photons forming a circumference with no red dot in the center of the circumference. For the sake of simplicity, this example is only dealing with one kind of detected particles by the tracking detectors, \"long tracks\". So don't worry if you see more circumferences, you are not daydreaming."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Using Convolutional Neural Networks for particle detection\n",
    "\n",
    "As we saw in the lectures, there is a manner of transforming this problem into something we can use our Convolutional Neural Networks knowledge for. Taking the previous event as an example, we are only interested in the _corona_ shape around a particle extrapolation, and its momentum:\n",
    "\n",
    "<img src=\"images/corona.png\" alt=\"corona\" width=\"800\" />\n",
    "\n",
    "If we convert the above corona to polars and normalize, we would end up with an image as the following:\n",
    "\n",
    "<img src=\"images/m15329_e0s84_pion.png\" alt=\"pion\" width=\"100\" />\n",
    "\n",
    "Note how the image is not a vertical line, but instead its shape is a bit distorted, due to the mirrors of the detector.\n",
    "\n",
    "One last transformation is applied to the above image: All its active pixels (1) are multiplied by the momentum of the particle, and the image is normalized so all its values are in the range [0-1].\n",
    "\n",
    "Finally, we are ready to identify the particle based on just looking at the image. We have transformed the RICH reconstruction problem into a classification problem of a 32x32 black and white image into one of six particles:\n",
    "\n",
    "* Pion\n",
    "* Muon\n",
    "* Electron\n",
    "* Kaon\n",
    "* Proton\n",
    "* Deuteron\n",
    "\n",
    "We are now ready to take on real RICH reconstruction using CNNs!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 3\n",
    "\n",
    "In contrast with previous exercises, here we will use some helper functions.\n",
    "\n",
    "Let's load some events and check how many particles of each kind we have in our datasets:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from instance import instance\n",
    "\n",
    "# Create instance of the problem\n",
    "i = instance(\n",
    "  balance=False,\n",
    "  base_containing_folder=\"/eos/user/d/dcampora/shared/rico/20190303_events/\",\n",
    "  image_size=32,\n",
    "  particle_ranges=((0.0, 0.005), (0.8, 0.805), (0.9, 0.905)),\n",
    "  image_format=\"float\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to solve the previous instance, we will employ a Convolutional Neural Network. The helper functions we are using require defining the keras model extending a base class.\n",
    "\n",
    "Let's define a very simple model:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Libraries for model\n",
    "from models.model import keras_model\n",
    "import keras\n",
    "from keras.models import Sequential\n",
    "from keras.layers import Dense, Dropout, Flatten, Activation\n",
    "from keras.layers import Conv2D, MaxPooling2D\n",
    "\n",
    "# Create a custom model\n",
    "class custom_simple_model(keras_model):\n",
    "  def __init__(self,\n",
    "    input_shape,\n",
    "    batch_size = 128,\n",
    "    num_classes = 6,\n",
    "    epochs = 100):\n",
    "    super(custom_simple_model, self).__init__(batch_size, num_classes, epochs)\n",
    "\n",
    "    self.model = Sequential()\n",
    "    self.model.add(Conv2D(32, (3, 3), padding='same',\n",
    "                     input_shape=input_shape))\n",
    "    self.model.add(Activation('relu'))\n",
    "    self.model.add(Conv2D(32, (3, 3), padding='same'))\n",
    "    self.model.add(Activation('relu'))\n",
    "    self.model.add(Conv2D(32, (3, 3), padding='same'))\n",
    "    self.model.add(Activation('relu'))\n",
    "\n",
    "    self.model.add(Flatten())\n",
    "    self.model.add(Dense(256))\n",
    "    self.model.add(Activation('relu'))\n",
    "    \n",
    "    self.model.add(Dropout(0.5))\n",
    "    self.model.add(Dense(num_classes))\n",
    "    self.model.add(Activation('softmax'))\n",
    "    \n",
    "    self.model.compile(loss=keras.losses.categorical_crossentropy,\n",
    "                  optimizer=keras.optimizers.Adadelta(),\n",
    "                  metrics=['accuracy'])\n",
    "    \n",
    "    # Add early stop regularization technique\n",
    "    early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0, patience=3, verbose=1, mode='auto')\n",
    "    self.callbacks.append(early_stop)\n",
    "\n",
    "  def __repr__(self):\n",
    "    return \"custom_simple_model\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we are ready to run our model with our instance:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Solve with model defined previously\n",
    "m = custom_simple_model(i.training_dataset.keras_input_shape, epochs=10)\n",
    "i.solve(m)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An executive summary in text form, and the weights of the learnt network, are saved to the folder `saved_models`.\n",
    "\n",
    "* Apply everything you know so far to obtain the best reconstruction efficiency. You should try to maximize the line labeled as `ID eff (%)`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
